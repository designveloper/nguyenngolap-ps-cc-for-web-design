# Photoshop CC for Web Design
Intern: Nguyen Ngo Lap

## 1. Core concepts
Remember:

- Think about UX (User Experience)
- Test the designs
    - Tools: Edge Reflow...
- Work with a developer
- Use feedback

Why use PTS?

- Faster generaton of web assets
- Visual representation of your projects
- Built-in CSS features
- Expanded vector tool set

Pain points of PTS for web:

- No multi-page workflow
- Unable to simulate responsive web design
- Code from CSS isn't always "clean"
- Many feel like you're doubling your design efforts

## 3. Designing responsively
3 ways to design mobile:

- Fluid - Build with percentages for widths
- Adaptive - Target specific devices with `media` queries
- Responsive - Built on a fluid grid that scales down
    - Breakpoint - minumum width and height before a layout change

### Progressive enhancement vs Graceful degradation:

- Progressive enhancement: Mobile first -> Larger screen sizes: tablets, desktops, etc, starting to add more content and esthetics to increase the usability on those devices
- Graceful degradation: Full on desktop first -> Take away and rearrange content to fit the site into smaller screens


What this means for PS:

- Consult the clients to determine mobile needs
- Work with a developer to select a design method
- Choose the workflow for building the site
- Deliver comps for each adaptation of the site
- Have a functioning prototype ready for the client

**References:**

- Responsive PSD Templates: http://blazrobar.com/
- https://helpx.adobe.com/photoshop/how-to/photoshop-linked-smart-objects.html

## 4. Creating a Wireframe
\- Grid systems: Choose the ones that can be easily translated into CSS

- 960 Grid System: http://960.gs/
    - Based on fixed width - 960px -> No consideration for responsive design
- Unsemantic: https://unsemantic.com/
    - Full responsive capabilities
- "A better PS grid for responsive web design": http://www.elliotjaystocks.com/blog/a-better-photoshop-grid-for-responsive-web-design/
- Lemonade: https://lemonade.im/
- 1200px Grid System: https://1200px.com/
- Simple Grid: http://thisisdallas.github.io/Simple-Grid/

## 5. Creating aesthetic elements

\- Adobe Color CC:  https://color.adobe.com/ (Hope the cracked version has it)
\- Where to find images for mockups: (avoid copyright)

- http://www.freeimages.com/
- http://www.istockphoto.com/
- http://www.thinkstockphotos.com/
- https://www.pond5.com/ (<-- Recheck)
- https://unsplash.com/ (Seems good)

## 6. Creating a UI Kit

\- Sample of free UI kits:

- https://dribbble.com/shots/947782-Freebie-PSD-Flat-UI-Kit
- https://dribbble.com/shots/1083847-Flat-UI-Kit-Free-PSD
- http://www.iceflowstudios.com/2013/premium/splash-of-color-premium-ui-kit/
- http://designmodo.github.io/Flat-UI/ **(<-- This)**

\- Icon fonts:

- Genericons
- Font Awesome
- Iconic
- Glyphicons

## 7. Assembling a page mockup
\- **(Important)** Creating hover effect (using layer comps): https://designm.ag/tutorials/hover-effects-for-ui-elements-in-photoshop/

## 8. Optimizing Web Graphics
### Image vs CSS

- Image slows loading time
- CSS is faster and more efficient

**What can't CSS do?**

- Photographs
- Complex graphics and/or logos
- Patterns (some)

**What CSS can do:**

- Simple logos
- Animations
- Background colors (especially gradient)
- Drop shadows & effects
- Icons and buttons

### Web file formats
#### Images

- JPG
    - Photos
    - Images not requiring transparency
    - Anything needing color fidelity
- GIF
    - Small animations
    - Icons (if not reproducible with icon fonts)
    - Areas of continuous color (no gradients)
    - Used rarely these days :-s
- PNG
    - Images with lots of transparency
    - Logos, icons, etc.
    - Saved in 24-bit for best results

### Adobe PS Generator Plugin
Allows quick export of multiple web graphics from a single PSD file, stores them in a folder that's continuously updated throughout the life of the file.

References: http://blogs.adobe.com/photoshop/2013/09/introducing-adobe-generator-for-photoshop-cc.html

### How and when to use SVG
(Scalable vector graphic)

Saved as EPS -> Open in AI.

## 10. Using other apps in the workflow

\- Creating CSS with PS: Apparently I need PS CC

\- Creating HTML with PS: Nope. It's not possible to generate a proper interactive HTML page (only one big image)

\- Edge Reflow: PS CC
    - http://www.adobe.com/products/edge-reflow.html

\- Muse: https://helpx.adobe.com/muse/how-to/what-is-muse.html